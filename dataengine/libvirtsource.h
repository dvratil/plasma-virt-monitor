/*
    Copyright (C) 2012  Dan Vratil <dan@progdan.cz>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef LIBVIRTSOURCE_H
#define LIBVIRTSOURCE_H

#include <plasma/datacontainer.h>
#include <libvirt/libvirt.h>

#include <QTimer>

class LibvirtSource : public Plasma::DataContainer
{
    Q_OBJECT

  public:
    explicit LibvirtSource(const virDomainPtr &domain, const virConnectPtr &connection, QObject* parent = 0);

    virtual ~LibvirtSource();

  public Q_SLOTS:
    void updateData();

  private Q_SLOTS:
    void dataUpdateRequested();
    void updateCPUUsage();

  private:
    virConnectPtr m_connection;
    QTimer *m_cpuUsageTimer;

    qint64 m_lastTimestamp;
    qulonglong m_prevCPUTime;
};

#endif // LIBVIRTSOURCE_H
