/*
    Copyright (C) 2012  Dan Vratil <dan@progdan.cz>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef LIBVIRTJOB_H
#define LIBVIRTJOB_H

#include <Plasma/ServiceJob>
#include <libvirt/libvirt.h>

class LibvirtJob: public Plasma::ServiceJob
{
    Q_OBJECT

  public:
    enum Actions {
        Start = 0,
        Pause,
        Resume,
        Reboot,
        Shutdown,
        ForceOff
    };

    LibvirtJob(const QString &service, const QString &operation,
               QMap< QString, QVariant > &parameters,
               const virConnectPtr &conn, QObject *parent = 0);
    virtual ~LibvirtJob();

  protected:
    void start();

  private:
    bool setState(const int &state);
    bool setCPUCount(const int &cpuCount);
    bool setMaxCPUCount(const int &cpuCount);
    bool setMemorySize(const int &memSize);
    bool setMaxMemorySize(const int &memSize);

    QString m_service;
    virConnectPtr m_conn;
};

#endif // LIBVIRTJOB_H
