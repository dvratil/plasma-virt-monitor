/*
    Copyright (C) 2012  Dan Vratil <dan@progdan.cz>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef LIBVIRTEVENTLOOP_H
#define LIBVIRTEVENTLOOP_H

#include <QThread>
#include <QMutex>
#include <libvirt/libvirt.h>

class LibvirtEventLoop: public QThread
{
    Q_OBJECT
  public:
    explicit LibvirtEventLoop(QObject* parent = 0);
    virtual ~LibvirtEventLoop();

    virtual void run();

  public Q_SLOTS:
    void quit();

 Q_SIGNALS:
     void eventCallback(const virConnectPtr &conn, const virDomainPtr &domain);

  private:
    QMutex m_stopMutex;
    bool m_stop;

    friend void lifecycleEventCb(virConnectPtr conn, virDomainPtr domain, int event, int detail, void *opaque);
    friend void rebootEventCb(virConnectPtr conn, virDomainPtr domain, int event, int detail, void *opaque);
    friend void watchdogEventCb(virConnectPtr conn, virDomainPtr domain, int event, int detail, void *opaque);
    friend void pmsuspendEventCb(virConnectPtr conn, virDomainPtr domain, int event, int detail, void *opaque);
    friend void pmwakeupEventCb(virConnectPtr conn, virDomainPtr domain, int event, int detail, void *opaque);
};

#endif // LIBVIRTEVENTLOOP_H
