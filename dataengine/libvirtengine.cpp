/*
    Copyright (C) 2012  Dan Vratil <dan@progdan.cz>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#include "libvirtengine.h"
#include "libvirtsource.h"
#include "libvirtservice.h"
#include "libvirteventloop.h"

#include <plasma/datacontainer.h>

LibvirtEngine::LibvirtEngine(QObject* parent, const QVariantList& args):
    Plasma::DataEngine(parent, args),
    m_eventThread(0)
{
    Q_UNUSED(args);

    setMinimumPollingInterval(1000);

    /* Initialize libvirt */
    virInitialize();
}

void LibvirtEngine::init()
{
    /* Start libvirt event loop */
    m_eventThread = new LibvirtEventLoop(this);
    m_eventThread->start();

    /* Event loop will emit a signal whenever something happens */
    connect(m_eventThread, SIGNAL(eventCallback(virConnectPtr,virDomainPtr)),
            this, SLOT(eventLoopEvent(virConnectPtr,virDomainPtr)));

    /* Connect to local libvirtd daemon */
    m_connection = virConnectOpen("qemu:///system");
    if (!m_connection)
        return;

    /* List all domains, active and inactive */
    int numActiveDomains = virConnectNumOfDomains(m_connection);
    int numInactiveDomains = virConnectNumOfDefinedDomains(m_connection);

    char **inactiveDomains = (char **) malloc(sizeof(char *) * numInactiveDomains);
    int *activeDomains = (int *) malloc(sizeof(int) * numActiveDomains);

    numActiveDomains = virConnectListDomains(m_connection, activeDomains, numActiveDomains);
    numInactiveDomains = virConnectListDefinedDomains(m_connection, inactiveDomains, numInactiveDomains);

    for (int i = 0 ; i < numActiveDomains ; i++) {
        virDomainPtr domain = virDomainLookupByID(m_connection, activeDomains[i]);
        addSource(new LibvirtSource(domain, m_connection, this));
    }

    for (int i = 0 ; i < numInactiveDomains ; i++) {
        virDomainPtr domain = virDomainLookupByName(m_connection, inactiveDomains[i]);
        addSource(new LibvirtSource(domain, m_connection, this));
        free(inactiveDomains[i]);
    }

    free(activeDomains);
    free(inactiveDomains);
}

LibvirtEngine::~LibvirtEngine()
{
    /* Stop the libvirt event loop */
    if (m_eventThread) {
        m_eventThread->quit();
        m_eventThread->wait();
        delete m_eventThread;
    }

    /* Unref libvirt connection */
    if (m_connection)
        virConnectClose(m_connection);
}

Plasma::Service* LibvirtEngine::serviceForSource(const QString& source)
{
    return new LibvirtService(source, m_connection, this);
}

void LibvirtEngine::eventLoopEvent(const virConnectPtr& conn, const virDomainPtr& domain)
{
    char uuid[VIR_UUID_STRING_BUFLEN];
    virDomainGetUUIDString(domain, uuid);

    /* Get container for given domain */
    LibvirtSource *source = dynamic_cast< LibvirtSource* >(this->containerForSource(uuid));
    if (!source) {
        virConnectClose(conn);
        virDomainFree(domain);
        return;
    }

    /* Force the container to update itself */
    emit source->updateData();
    scheduleSourcesUpdated();

    /* Unref the connection and domain which were ref'ed in the event loop */
    virConnectClose(conn);
    virDomainFree(domain);
}


K_EXPORT_PLASMA_DATAENGINE(libvirt, LibvirtEngine);