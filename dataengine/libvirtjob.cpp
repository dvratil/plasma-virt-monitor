/*
    Copyright (C) 2012  Dan Vratil <dan@progdan.cz>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#include "libvirtjob.h"

#include <KDebug>

LibvirtJob::LibvirtJob(const QString &service, const QString &operation,
                       QMap< QString, QVariant > &parameters,
                       const virConnectPtr &conn, QObject* parent):
    Plasma::ServiceJob(parent->objectName(), operation, parameters, parent),
    m_service(service),
    m_conn(conn)
{
    virConnectRef(m_conn);
}

LibvirtJob::~LibvirtJob()
{
    virConnectClose(m_conn);
}

void LibvirtJob::start()
{
    const QString operation = operationName();
    bool result;
    QString reason;

    if (operation == "setState") {
        result = setState(parameters().value("action").toInt());
        reason = i18n("Failed to change domain state");
    } else if (operation == "setCPUCount") {
        result = setCPUCount(parameters().value("count").toInt());
        reason = i18n("Failed to change CPU count");
    } else if (operation == "setMaxCPUCount") {
        result = setMaxCPUCount(parameters().value("count").toInt());
        reason = i18n("Failed to set CPU limit");
    } else if (operation == "setMemorySize") {
        result = setMemorySize(parameters().value("sizeKb").toInt());
        reason = i18n("Failed to change memory size");
    } else if (operation == "setMaxMemorySize") {
        result = setMaxMemorySize(parameters().value("sizeKb").toInt());
        reason = i18n("Failed to change memory limit");
    } else {
        result = false;
        reason = i18n("Unknown operation");
    }


    setResult(result);
    if (!result)
        setErrorText(reason);

    return;
}

bool LibvirtJob::setState(const int& action)
{
    virDomainPtr domain = virDomainLookupByUUIDString(m_conn, m_service.toLatin1().constData());
    if (!domain)
        return false;

    switch (action) {
        case Start:
            return (virDomainCreateWithFlags(domain, 0) == 0);

        case Pause:
            return (virDomainSuspend(domain) == 0);

        case Resume:
            return (virDomainResume(domain) == 0);

        case Reboot:
            return (virDomainReboot(domain, 0) == 0);

        case Shutdown:
            return (virDomainShutdown(domain) == 0);

        case ForceOff:
            return (virDomainDestroy(domain) == 0);
    }

    return false;
}

bool LibvirtJob::setCPUCount(const int& cpuCount)
{
    virDomainPtr domain = virDomainLookupByUUIDString(m_conn, m_service.toLatin1().constData());
    if (!domain)
        return false;

    int state;
    if (virDomainGetState(domain, &state, NULL, 0) != 0)
        return false;

    int flags = VIR_DOMAIN_AFFECT_CONFIG;
    if (state == VIR_DOMAIN_RUNNING)
        flags |= VIR_DOMAIN_AFFECT_LIVE;

    return (virDomainSetVcpusFlags(domain, cpuCount, flags) == 0);
}

bool LibvirtJob::setMaxCPUCount(const int& cpuCount)
{
    virDomainPtr domain = virDomainLookupByUUIDString(m_conn, m_service.toLatin1().constData());
    if (!domain)
        return false;

    return (virDomainSetVcpusFlags(domain, cpuCount, VIR_DOMAIN_AFFECT_CONFIG | VIR_DOMAIN_VCPU_MAXIMUM) == 0);
}


bool LibvirtJob::setMemorySize(const int& memSize)
{
    virDomainPtr domain = virDomainLookupByUUIDString(m_conn, m_service.toLatin1().constData());
    if (!domain)
        return false;

    int state;
    if (virDomainGetState(domain, &state, NULL, 0) != 0)
        return false;

    int flags = VIR_DOMAIN_AFFECT_CONFIG;
    if (state == VIR_DOMAIN_RUNNING)
        flags |= VIR_DOMAIN_AFFECT_LIVE;

    return (virDomainSetMemoryFlags(domain, memSize, flags) == 0);
}

bool LibvirtJob::setMaxMemorySize(const int& memSize)
{
    virDomainPtr domain = virDomainLookupByUUIDString(m_conn, m_service.toLatin1().constData());
    if (!domain)
        return false;

    return (virDomainSetMaxMemory(domain, memSize) == 0);
}
