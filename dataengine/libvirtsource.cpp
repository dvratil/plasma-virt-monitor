/*
        Copyright (C) 2012  Dan Vratil <dan@progdan.cz>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#include "libvirtsource.h"
#include <libvirt/virterror.h>
#include <QDateTime>
#include <QtGlobal>
#include <QThread>
#include <QtXml/QDomDocument>
#include <QtXml/QDomElement>

#include <math.h>

#include <KDebug>
#include <sys/socket.h>

LibvirtSource::LibvirtSource(const virDomainPtr& domain, const virConnectPtr& connection, QObject* parent):
    Plasma::DataContainer(parent),
    m_connection(connection),
    m_lastTimestamp(0),
    m_prevCPUTime(0)
{
    virConnectRef(m_connection);

    /* Use UUID as source name */
    char buf[VIR_UUID_STRING_BUFLEN];
    virDomainGetUUIDString(domain, buf);
    setObjectName(buf);

    m_cpuUsageTimer = new QTimer();
    connect(m_cpuUsageTimer, SIGNAL(timeout()),
            this, SLOT(updateCPUUsage()));

    /* Initialization */
    setData("CPUUsagePerc", 0);
    dataUpdateRequested();
}

LibvirtSource::~LibvirtSource()
{
    virConnectClose(m_connection);

    delete m_cpuUsageTimer;
}

void LibvirtSource::updateData()
{
    dataUpdateRequested();
}


void LibvirtSource::dataUpdateRequested()
{
    virDomainPtr domain = virDomainLookupByUUIDString(m_connection, objectName().toLatin1().constData());
    setData("Name", virDomainGetName(domain));
    setData("ID", (int) virDomainGetID(domain));
    setData("OSType", virDomainGetOSType(domain));

    int state, stateReason;
    virDomainGetState(domain, &state, &stateReason, 0);
    setData("State", state);
    setData("StateReason", stateReason);

    setData("MemSizeKb", 0);
    setData("MaxMemSizeKb", 0);

    char *xml = virDomainGetXMLDesc(domain, 0);
    QDomDocument doc;
    doc.setContent(QString(xml));
    QDomElement xmlDomain = doc.firstChildElement("domain");
    if (xmlDomain.isElement()) {
        QDomNodeList nodes = xmlDomain.elementsByTagName("memory");
        if (nodes.length() == 1)
            setData("MaxMemSizeKb", nodes.at(0).toElement().text().toUInt());

        nodes = doc.elementsByTagName("currentMemory");
        if (nodes.length() == 1)
            setData("MemSizeKb", nodes.at(0).toElement().text().toUInt());

        /* virtDomainGetMetaData() for description throws a runtime warning when 
         * domain has no description set. */
        nodes = doc.elementsByTagName("description");
        if (nodes.length() == 1) {
            setData("Description", nodes.at(0).toElement().text());
        } else {
            setData("Description", "");
        }
    }
    free (xml);

    virDomainInfoPtr domainInfo = new virDomainInfo;
    virDomainGetInfo(domain, domainInfo);
    setData("CPUCount", domainInfo->nrVirtCpu);
    setData("MaxCPUCount", virDomainGetVcpusFlags(domain, VIR_DOMAIN_VCPU_CONFIG | VIR_DOMAIN_VCPU_MAXIMUM));
    delete domainInfo;


    if (state == VIR_DOMAIN_RUNNING) {
        if (!m_cpuUsageTimer->isActive())
            m_cpuUsageTimer->start(1000);
    } else {
        m_cpuUsageTimer->stop();
        setData("CPUUsagePerc", 0);
    }

    checkForUpdate();
}

void LibvirtSource::updateCPUUsage()
{
    virDomainPtr domain = virDomainLookupByUUIDString(m_connection, objectName().toLatin1().constData());

    virDomainInfoPtr domainInfo = new virDomainInfo;
    virDomainGetInfo(domain, domainInfo);

    if (m_prevCPUTime == 0)
        m_prevCPUTime = domainInfo->cpuTime;
    qulonglong cpuTime = domainInfo->cpuTime - m_prevCPUTime;
    m_prevCPUTime = domainInfo->cpuTime;

    qint64 now = QDateTime::currentDateTime().toMSecsSinceEpoch();
    float totalUsage = 0;
    if (m_lastTimestamp != 0)
            totalUsage = ((cpuTime * 100.0) / ((now - m_lastTimestamp) * 1000.0 * 1000.0));
    m_lastTimestamp = now;

    double usage = totalUsage / domainInfo->nrVirtCpu;
    setData("CPUUsagePerc", (int) qMax(0.0, qMin(100.0, usage)));

    delete domainInfo;
    checkForUpdate();
}
