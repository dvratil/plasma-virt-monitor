/*
    Copyright (C) 2012  Dan Vratil <dan@progdan.cz>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef LIBVIRTENGINE_H
#define LIBVIRTENGINE_H

#include <plasma/dataengine.h>
#include <libvirt/libvirt.h>

class LibvirtEventLoop;

class LibvirtEngine: public Plasma::DataEngine
{
    Q_OBJECT

  public:
    LibvirtEngine(QObject* parent, const QVariantList& args);
    virtual ~LibvirtEngine();

    virtual Plasma::Service* serviceForSource(const QString& source);

  protected:
    void init();

  private Q_SLOTS:
    void eventLoopEvent(const virConnectPtr &conn, const virDomainPtr &domain);

  private:
    virConnectPtr m_connection;
    LibvirtEventLoop *m_eventThread;


};

#endif // LIBVIRTENGINE_H
