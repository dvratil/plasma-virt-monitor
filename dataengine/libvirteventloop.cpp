/*
    Copyright (C) 2012  Dan Vratil <dan@progdan.cz>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#include "libvirteventloop.h"

#include <QMetaType>
#include <KDebug>

#include <libvirt/libvirt.h>
#include <libvirt/virterror.h>

/* Libvirt will allow us to connect every callback only once, no matter to which event
 * it is actually connected, therefor we must have a special callback for each
 * event we want to listen to */
void lifecycleEventCb(virConnectPtr conn, virDomainPtr domain, int event, int detail, void *opaque)
{
    Q_UNUSED (conn);
    Q_UNUSED (event);
    Q_UNUSED (detail);

    /* Signals emitted accross threads are queued (asynchronous) and libvirt
     * guarantees existence of the pointers only until this callback returns,
     * so we must make sure they will be valid even in the slot where we can
     * unref them.
     * Note: this might lead to troubles if there were multiple slots connected
     * to this signal. */
    virConnectRef(conn);
    virDomainRef(domain);

    /* Notify the engine that something has happened */
    emit ((LibvirtEventLoop *) opaque)->eventCallback(conn, domain);
}

void rebootEventCb(virConnectPtr conn, virDomainPtr domain, int event, int detail, void *opaque)
{
    Q_UNUSED (conn);
    Q_UNUSED (event);
    Q_UNUSED (detail);

    virConnectRef(conn);
    virDomainRef(domain);
    emit ((LibvirtEventLoop *) opaque)->eventCallback(conn, domain);
}

void watchdogEventCb(virConnectPtr conn, virDomainPtr domain, int event, int detail, void *opaque)
{
    Q_UNUSED (conn);
    Q_UNUSED (event);
    Q_UNUSED (detail);

    virConnectRef(conn);
    virDomainRef(domain);
    emit ((LibvirtEventLoop *) opaque)->eventCallback(conn, domain);
}

void pmsuspendEventCb(virConnectPtr conn, virDomainPtr domain, int event, int detail, void *opaque)
{
    Q_UNUSED (conn);
    Q_UNUSED (event);
    Q_UNUSED (detail);

    virConnectRef(conn);
    virDomainRef(domain);
    emit ((LibvirtEventLoop *) opaque)->eventCallback(conn, domain);
}

void pmwakeupEventCb(virConnectPtr conn, virDomainPtr domain, int event, int detail, void *opaque)
{
    Q_UNUSED (conn);
    Q_UNUSED (event);
    Q_UNUSED (detail);

    virConnectRef(conn);
    virDomainRef(domain);
    emit ((LibvirtEventLoop *) opaque)->eventCallback(conn, domain);
}

LibvirtEventLoop::LibvirtEventLoop(QObject* parent):
    QThread(parent),
    m_stop(false)
{
    /* Signals between threads are queued, therefor all signal parameters
     * must be reqistered to Qt MetaType system */
    qRegisterMetaType<virConnectPtr>("virConnectPtr");
    qRegisterMetaType<virDomainPtr>("virDomainPtr");
}

LibvirtEventLoop::~LibvirtEventLoop()
{

}

void LibvirtEventLoop::run()
{
    /* Initialize libvirt event loop */
    virEventRegisterDefaultImpl();

    /* Setup separate connection */
    virConnectPtr connection = virConnectOpen("qemu:///system");

    /* Make sure the connection stays alive in case there were no events for some time */
    if (virConnectSetKeepAlive(connection, 5, 3) < 0) {
        virConnectClose(connection);
        return;
    }

    /* List to some interesting events */
    QList< int > handlerIDs;
    handlerIDs << virConnectDomainEventRegisterAny(connection, NULL, VIR_DOMAIN_EVENT_ID_LIFECYCLE,
                                     VIR_DOMAIN_EVENT_CALLBACK (lifecycleEventCb), this, NULL);
    handlerIDs << virConnectDomainEventRegisterAny(connection, NULL, VIR_DOMAIN_EVENT_ID_REBOOT,
                                     VIR_DOMAIN_EVENT_CALLBACK (rebootEventCb), this, NULL);
    handlerIDs << virConnectDomainEventRegisterAny(connection, NULL, VIR_DOMAIN_EVENT_ID_WATCHDOG,
                                     VIR_DOMAIN_EVENT_CALLBACK (watchdogEventCb), this, NULL);
    handlerIDs << virConnectDomainEventRegisterAny(connection, NULL, VIR_DOMAIN_EVENT_ID_PMSUSPEND,
                                     VIR_DOMAIN_EVENT_CALLBACK (pmsuspendEventCb), this, NULL);
    handlerIDs << virConnectDomainEventRegisterAny(connection, NULL, VIR_DOMAIN_EVENT_ID_PMWAKEUP,
                                     VIR_DOMAIN_EVENT_CALLBACK (pmwakeupEventCb), this, NULL);
    m_stopMutex.lock();
    while (!m_stop) {
        m_stopMutex.unlock();


        /* Die another day */
        if (virConnectIsAlive(connection) != 1)
            break;

        /* Process all queued libvirt events */
        if (virEventRunDefaultImpl() < 0) {
            m_stop = true;
        }

        m_stopMutex.lock();
    }
    m_stopMutex.unlock();

    /* Disconnect event handlers */
    foreach (const int &id, handlerIDs) {
        virConnectDomainEventDeregisterAny(connection, id);
    }

    /* Disconnect from server */
    virConnectClose(connection);

}


void LibvirtEventLoop::quit()
{
    m_stopMutex.lock();
    m_stop = true;
    m_stopMutex.unlock();
}
