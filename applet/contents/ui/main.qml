/*
    Copyright (C) 2012  Dan Vratil <dan@progdan.cz>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

import Qt 4.7
import org.kde.plasma.core 0.1 as PlasmaCore
import org.kde.plasma.components 0.1 as PlasmaComponents
import org.kde.plasma.graphicslayouts 4.7 as GraphicsLayouts

Item {
    id: root;
    width: 250;
    height: 300;

    PlasmaComponents.PageStack {
        id: stack;
        initialPage: domainsListPage;
        width: parent.width;
        height: parent.height;

        DomainsListPage {
            id: domainsListPage;
            stack: stack;

            height: parent.height;
            width: parent.width;
        }

        DomainDetailsPage {
            id: domainDetailsPage;
            stack: stack;

            height: parent.height
            width: parent.width;
        }
    }

    PlasmaCore.DataSource {
        id: dataSource;
        dataEngine: "libvirt";
        connectedSources: [dataSource.sources];
        interval: 1000;

        onSourceAdded: {
            domainsListPage.addDomain(source, data[source]);
        }

        onSourceRemoved: {
            domainsListPage.removeDomain(source);
        }

        onNewData: {
            domainsListPage.updateDomain(sourceName, dataSource.data[sourceName]);
        }

        Component.onCompleted: {
            dataSource.connectedSources = dataSource.sources;
        }
    }

}


