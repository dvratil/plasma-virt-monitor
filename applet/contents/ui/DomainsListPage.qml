/*
    Copyright (C) 2012  Dan Vratil <dan@progdan.cz>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

import Qt 4.7
import org.kde.plasma.core 0.1 as PlasmaCore
import org.kde.plasma.components 0.1 as PlasmaComponents
import "domain.js" as Domain;

PlasmaComponents.Page {
    id: page;

    property QtObject stack;

    ListView {
        id: domainsList;

        width: parent.width;
        height: parent.height;

        model: ListModel { }
        delegate: DomainDelegate {}

        highlightFollowsCurrentItem: true;
        highlight: PlasmaComponents.Highlight {
            id: highlight;
            width: parent.width;
            height: parent.height;
            hover: true;
        }
        focus: true;
        clip: true;

        MouseArea {
            z: 1
            anchors.fill: parent;
            hoverEnabled: true;

            onClicked: {
                domainsList.currentIndex = domainsList.indexAt(mouse.x, mouse.y);
                stack.push(domainDetailsPage);
                var curItem = domainsList.model.get(domainsList.currentIndex);
                Domain.updateObjectData(domainDetailsPage, curItem.uuid,  dataSource.data[curItem.uuid]);
                mouse.accepted = true;
            }
            onPositionChanged: {
                domainsList.currentIndex = domainsList.indexAt(mouse.x + domainsList.contentX, mouse.y + domainsList.contentY);
                mouse.accepted = false;
            }
        }

    }

    function addDomain(uuid, data)
    {
        domainsList.model.append({
            uuid: uuid,
            name: data["Name"],
            description: data["Description"],
            domainID: data["ID"],
            domainState: data["State"],
            stateReason: data["StateReason"],
            memSize: data["MemSizeKb"],
            maxMemSize: data["MaxMemSizeKb"],
            cpuCount: data["CPUCount"],
            maxCPUCount: data["MaxCPUCount"],
            cpuUsage: data["CPUUsagePerc"]
        });
    }

    function updateDomain(uuid, data)
    {
        model = domainsList.model;
        for (i = 0; i < model.count; i++) {
            if (model.get(i).uuid == uuid) {
                model.setProperty(i, "name", data["Name"]);
                model.setProperty(i, "description", data["Description"]);
                model.setProperty(i, "domainID", data["ID"]);
                model.setProperty(i, "domainState", data["State"]);
                model.setProperty(i, "stateReason", data["StateReason"]);
                model.setProperty(i, "memSize", data["MemSizeKb"]);
                model.setProperty(i, "maxMemSize", data["MaxMemSizeKb"]);
                model.setProperty(i, "cpuCount", data["CPUCount"]);
                model.setProperty(i, "maxCPUCount", data["MaxCPUCount"]);
                model.setProperty(i, "cpuUsage", data["CPUUsagePerc"]);

                if ((stack.currentPage == domainDetailsPage) && (domainDetailsPage.uuid == uuid)) {
                    Domain.updateObjectData(domainDetailsPage, uuid, data);
                }

                return;
            }
        }

        addDomain(uuid, data);
    }

    function removeDomain(uuid)
    {
        for (i = 0; i < domainsList.model.count; i++) {
            if (domainsList.model.get(i).uuid == uuid) {
                domainsList.model.remove(i);
                return;
            }
        }
    }
}


