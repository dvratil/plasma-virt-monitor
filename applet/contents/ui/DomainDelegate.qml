/*
    Copyright (C) 2012  Dan Vratil <dan@progdan.cz>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

import Qt 4.7
import org.kde.plasma.components 0.1 as PlasmaComponents
import "domain.js" as Domain

Column {
    id: domainDelegate;
    width: parent.width;

    Item {
        id: button;
        height: 50
        width: parent.width;

        PlasmaComponents.Label {
            id: nameLabel;
            text: name;
            elide: Text.ElideRight;
            height: parent.height;
            anchors {
                left: parent.left;
                top: parent.top;
                bottom: parent.bottom;
                right: stateLabel.left;
                leftMargin: 10;
            }
        }

        PlasmaComponents.Label {
            id: stateLabel;
            text: Domain.getDomainState(domainState)
            height: parent.height;
            anchors {
                right: parent.right;
                verticalCenter: parent.verticalCenter;
                rightMargin: 5;
            }
        }
    }
}
