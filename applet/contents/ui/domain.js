/*
    Copyright (C) 2012  Dan Vratil <dan@progdan.cz>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

var DomainState = { "Unknown":      0,
                    "Running":      1,
                    "Blocked":      2,
                    "Paused":       3,
                    "ShuttingDown": 4,
                    "ShutOff":      5,
                    "Crashed":      6,
                    "Suspended":    7
                  };

var DomainAction = { "Start" :      0,
                     "Pause" :      1,
                     "Resume":      2,
                     "Reboot":      3,
                     "Shutdown":    4,
                     "ForceOff":    5
                   };

var DomainRunningReason = { "Unknown":              0,
                            "Booted":               1,
                            "Migrated":             2,
                            "RestoredFile":         3,
                            "RestoredSnapshot":     4,
                            "Unpaused":             5,
                            "MigrationCancelled":   6,
                            "SaveCancelled":        7,
                            "Woken":                8,
                          };

var DomainBlockedReason = { "Unknown":  0 };

var DomainPausedReason = { "Unknown":   0,
                           "User":      1,
                           "Migration": 2,
                           "Save":      3,
                           "CoreDump":  4,
                           "IO":        5,
                           "Watchdog":  6,
                           "Restored":  7,
                           "Shutdown":  8
                         };

var DomainShuttingOffReason = { "Unknown": 0,
                                "User":    1
                              };


var DomainShutdownReason = { "Unknown":          0,
                             "Normal":           1,
                             "Forced":           2,
                             "Crashed":          3,
                             "Migrated":         4,
                             "Saved":            5,
                             "StartFailed":      6,
                             "RestoredShutdown": 7
                           };

var DomainCrashedReason = { "Unknown": 0 };

var DomainSuspendedReason = { "Unknown": 0 };


function getDomainState(state)
{
    switch (state) {
        case DomainState.Running:
            return i18n("Running");

        case DomainState.Blocked:
            return i18n("Blocked");

        case DomainState.Paused:
            return i18n("Paused");

        case DomainState.ShuttingDown:
            return i18n("Shutting down");

        case DomainState.ShutOff:
            return i18n("Shut off");

        case DomainState.Crashed:
            return i18n("Crashed");

        case DomainState.Suspended:
            return i18n("Suspended");

        default:
            return i18n("Unknown state");
    }
}



function getDomainStateReason(state, stateReason)
{
    switch (state) {

        case DomainState.Running:
            switch (stateReason) {
                case DomainRunningReason.Booted:
                    return i18n("Booted");
                case DomainRunningReason.Migrated:
                    return i18n("Migrated");
                case DomainRunningReason.RestoredFile:
                    return i18n("Restored from a state file");
                case DomainRunningReason.RestoredSnapshot:
                    return i18n("Restored from a snapshot");
                case DomainRunningReason.Unpaused:
                    return i18n("Unpaused");
                case DomainRunningReason.MigrationCancelled:
                    return i18n("Migration cancelled");
                case DomainRunningReason.SaveCancelled:
                    return i18n("Save canceled");
                case DomainRunningReason.Woken:
                    return i18n("Woken from suspend");
                default:
                    return i18n("Unknown reason");
            }

        case DomainState.Blocked:
            return i18n("Unknown reason");

        case DomainState.Paused:
            switch (stateReason) {
                case DomainPausedReason.User:
                    return i18n("Paused by user");
                case DomainPausedReason.Migration:
                    return i18n("Paused for offline migration");
                case DomainPausedReason.Save:
                    return i18n("Paused for save");
                case DomainPausedReason.CoreDump:
                    return i18n("Paused for offline core dump");
                case DomainPausedReason.IO:
                    return i18n("Paused toe to a disk I/O error");
                case DomainPausedReason.Watchdog:
                    return i18n("Paused due to a watchdog event");
                case DomainPausedReason.Restored:
                    return i18n("Paused after restoring from snapshot");
                case DomainPausedReason.Shutdown:
                    return i18n("Paused during shutdown process");
                default:
                    return i18n("Unknown reason");
            }

        case DomainState.ShuttingDown:
            switch (stateReason) {
                case ShuttingDownReason:
                    return i18n("Shutting down on user request");
                default:
                    return i18n("Unknown reason");
            }


        case DomainState.ShutOff:
            switch (stateReason) {
                case DomainShutdownReason.Normal:
                    return i18n("Normal shutdown");
                case DomainShutdownReason.Forceed:
                    return i18n("Forced shutdown");
                case DomainShutdownReason.Crashed:
                    return i18n("Domain crashed");
                case DomainShutdownReason.Migrated:
                    return i18n("Migrated to another host");
                case DomainShutdownReason.Saved:
                    return i18n("Saved to a file");
                case DomainShutdownReason.SartFailed:
                    return i18n("Domain failed to start");
                case DomainShutdownReason.RestoredShutdown:
                    return i18n("Restored from snapshot taken while domain was shutting down");
                default:
                    return i18n("Unknown reason");
            }

        case DomainState.Crashed:
        case DomainState.Migrated:
        default:
            return i18n("Unknown reason");
    }
}


function updateObjectData(object, uuid, data)
{
    object.uuid = uuid;
    object.name = data["Name"];
    object.description = data["Description"];
    object.domainID = data["ID"];
    object.domainState = data["State"];
    object.stateReason = data["StateReason"];
    object.memSize = data["MemSizeKb"];
    object.maxMemSize = data["MaxMemSizeKb"];
    object.cpuCount = data["CPUCount"];
    object.maxCPUCount = data["MaxCPUCount"];
    object.cpuUsage = data["CPUUsagePerc"];
}

function setDomainState(domainUUID, action)
{
    var service = dataSource.serviceForSource(domainUUID);
    var operation = service.operationDescription("setState");
    operation.action = action;

    var job = service.startOperationCall(operation);
}

function setCPUCount(domainUUID, cpuCount)
{
    var service = dataSource.serviceForSource(domainUUID);
    var operation = service.operationDescription("setCPUCount");
    operation.count = cpuCount;

    var job = service.startOperationCall(operation);
}

function setMemorySize(domainUUID, memorySize)
{
    var service = dataSource.serviceForSource(domainUUID);
    var operation = service.operationDescription("setMemorySize");
    operation.sizeKb = memorySize;

    var job = service.startOperationCall(operation);
}

function setMaxCPUCount(domainUUID, cpuCount)
{
    var service = dataSource.serviceForSource(domainUUID);
    var operation = service.operationDescription("setMaxCPUCount");
    operation.count = cpuCount;

    var job = service.startOperationCall(operation);
}

function setMaxMemorySize(domainUUID, memorySize)
{
    var service = dataSource.serviceForSource(domainUUID);
    var operation = service.operationDescription("setMaxMemorySize");
    operation.sizeKb = memorySize;

    var job = service.startOperationCall(operation);
}