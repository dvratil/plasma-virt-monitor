/*
    Copyright (C) 2012  Dan Vratil <dan@progdan.cz>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

import Qt 4.7
import org.kde.plasma.core 0.1 as PlasmaCore
import org.kde.plasma.components 0.1
import "domain.js" as Domain

Page {
    id: page;

    property string uuid: "";
    property string name: "";
    property string description: "";
    property int domainID: -1;
    property int domainState: -1;
    property int stateReason: -1;
    property int memSize: -1;
    property int maxMemSize: -1;
    property int cpuCount: -1;
    property int maxCPUCount: -1;
    property int cpuUsage: -1;
    property QtObject stack;


    Label {
        id: nameLabel;

        anchors {
            top: parent.top;
            left: parent.left;
            right: parent.right;
            horizontalCenter: parent.horizontalCenter;
        }

        font {
            bold: true;
            pointSize: 20;
        }

        text: name;
        elide: Text.ElideRight;
        horizontalAlignment: Text.AlignHCenter;
    }

    Label {
        id: descriptionLabel;
        wrapMode: Text.WordWrap;
        text: description;
        horizontalAlignment: Text.AlignHCenter;

        anchors {
            top: nameLabel.bottom;
            left: parent.left;
            right: parent.right;
            horizontalCenter: parent.horizontalCenter;
        }
    }

    Grid {
        anchors {
            horizontalCenter: parent.horizontalCenter;
            verticalCenter: parent.verticalCenter;
        }
        columns: 2;
        spacing: 5;

        Label {
            id: idLabel;
            text: i18n("ID :");
            width: parent.width / 2;
            horizontalAlignment: Text.AlignRight;
        }
        Label {
            id: idValue;
            text: name;
        }

        Label {
            id: stateLabel;
            text: i18n("Status :");
            width: parent.width / 2;
            horizontalAlignment: Text.AlignRight;
        }
        ToolButton {
            id: stateValue;
            text: Domain.getDomainState(domainState);
            flat: false;

            onClicked: {
                var contextMenu;
                if (domainState == Domain.DomainState.Running)
                    contextMenu = runningDomainContextMenu.createObject(stateValue);
                else if (domainState == Domain.DomainState.Paused)
                    contextMenu = pausedDomainContextMenu.createObject(stateValue);
                else if (domainState == Domain.DomainState.ShutOff)
                    contextMenu = shutOffDomainContextMenu.createObject(stateValue);
                else
                    return;

                contextMenu.open();
            }
        }

        Component {
            id: runningDomainContextMenu;
            ContextMenu {
                visualParent: stateValue;
                MenuItem {
                    text: i18n("Pause");
                    onClicked: {
                        Domain.setDomainState(uuid, Domain.DomainAction.Pause);
                        stateValue.text = i18n("Pausing");
                        stateValue.enabled = false;
                    }
                }
                MenuItem {
                    text: i18n("Reboot");
                    onClicked: {
                        Domain.setDomainState(uuid, Domain.DomainAction.Reboot);
                        stateValue.text = i18n("Rebooting");
                        stateValue.enabled = false;
                    }
                }
                MenuItem {
                    text: i18n("Shutdown");
                    onClicked: {
                        Domain.setDomainState(uuid, Domain.DomainAction.Shutdown);
                        stateValue.text = i18n("Shutting down");
                        stateValue.enabled = false;
                    }
                }
                MenuItem {
                    text: i18n("Force Off");
                    onClicked: {
                        Domain.setDomainState(uuid, Domain.DomainAction.ForceOff);
                        stateValue.text = i18n("Shut off");
                        stateValue.enabled = false;
                    }
                }
            }
        }

        Component {
            id: pausedDomainContextMenu;
            ContextMenu {
                visualParent: stateValue;
                MenuItem {
                    text: i18n("Resume");
                    onClicked: {
                        Domain.setDomainState(uuid, Domain.DomainAction.Resume);
                        stateValue.text = i18n("Resuming");
                        stateValue.enabled = false;
                    }
                }
                MenuItem {
                    text: i18n("Force off");
                    onClicked: {
                        Domain.setDomainState(uuid, Domain.DomainAction.ForceOff);
                        stateValue.text = i18n("Shut off");
                        stateValue.enabled = false;
                    }
                }
            }
        }

        Component {
            id: shutOffDomainContextMenu;
            ContextMenu {
                visualParent: stateValue;
                MenuItem {
                    text: i18n("Start");
                    onClicked: {
                        Domain.setDomainState(uuid, Domain.DomainAction.Start);
                        stateValue.text = i18n("Booting");
                        stateValue.enabled = false;
                    }
                }
            }
        }


        Label {
            id: stateReasonLabel;
            text: i18n("Status Reason :");
            width: parent.width / 2;
            horizontalAlignment: Text.AlignRight;
        }
        Label {
            id: stateReasonValue;
            text: Domain.getDomainStateReason(domainState, stateReason);
        }

        Label {
            id: memSizeLabel;
            text: i18n("Memory:");
            horizontalAlignment: Text.AlignRight;
            width: parent.width / 2;
        }
        SpinBox {
            id: memSizeValue;
            step: 1;
            min: 1;
            suffix: " MB";
            value: 1;
            width: 60;
            height: 30;

            onChanged: {
                Domain.setMemorySize(uuid, value * 1024);
            }
        }

        Label {
            id: maxMemSizeLabel;
            text: i18n("Max Memory:");
            horizontalAlignment: Text.AlignRight;
            width: parent.width / 2;
        }
        SpinBox {
            id: maxMemSizeValue;
            min: memSizeValue.value;
            step: 1;
            suffix: " MB";
            value: 1;
            width: 60;
            height: 30;

            onChanged: {
                Domain.setMaxMemorySize(uuid, value * 1024);
            }
        }

        Label {
            id: cpuCountLabel;
            text: i18n("CPU Count :");
            width: parent.width / 2;
            horizontalAlignment: Text.AlignRight;
        }
        SpinBox {
            id: cpuCountValue;
            step: 1;
            min: 1;
            value: 1;
            width: 40;
            height: 30;

            onChanged: {
                Domain.setCPUCount(uuid, value);
            }
        }

        Label {
            id: maxCPUCountLabel;
            text: i18n("Max CPU Count :");
            width: parent.width / 2;
            horizontalAlignment: Text.AlignRight;
        }
        SpinBox {
            id: maxCPUCountValue;
            step: 1;
            min: cpuCountValue.value;
            value: 1;
            max: 128;
            width: 40;
            height: 30;

            onChanged: {
                Domain.setMaxCPUCount(uuid, value);
            }
        }

        Label {
            id: cpuUsageLabel;
            text: i18n("CPU Usage: ");
            width: parent.width / 2;
            horizontalAlignment: Text.AlignRight;
        }
        Label {
            id: cpuUsageValue;
            text: i18n("unavailable");
        }

    }

    ToolButton {
        id: backButton;
        height: 50;
        width: parent.width;
        text: "Back";
        flat: false;

        anchors {
            right: parent.right;
            left: parent.left;
            bottom: parent.bottom;
        }

        onClicked: {
            stack.pop();
        }
    }

    onNameChanged: nameLabel.text = name;
    onDomainIDChanged: idValue.text = domainID;
    onDomainStateChanged: {
        stateValue.text = Domain.getDomainState(domainState);
        stateValue.enabled = true;
        if (domainState != Domain.DomainState.Running)
            cpuUsageValue.text = i18n("unavailable");
    }
    onStateReasonChanged: {
        stateReasonValue.text = Domain.getDomainStateReason(domainState, stateReason);
        maxMemSizeValue.enabled = (domainState != Domain.DomainState.Running);
    }
    onMemSizeChanged: memSizeValue.value = parseInt(memSize) / 1024;
    onMaxMemSizeChanged: {
        maxMemSizeValue.value = parseInt(maxMemSize) / 1024;
        memSizeValue.max = maxMemSizeValue.value;
    }
    onCpuCountChanged: cpuCountValue.value = cpuCount;
    onMaxCPUCountChanged: {
        maxCPUCountValue.value = maxCPUCount;
        cpuCountValue.max = maxCPUCountValue.value;
    }
    onCpuUsageChanged: {
        if (domainState == Domain.DomainState.Running) 
            cpuUsageValue.text = parseInt(cpuUsage) + "%";
        else
            cpuCountValue.text = i18n("unavailable");
    }
}
