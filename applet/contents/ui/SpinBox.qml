/*
        Copyright (C) 2012  Dan Vratil <dan@progdan.cz>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

import Qt 4.7
import org.kde.plasma.components 0.1 as PlasmaComponents

Item {

    property int value: 0;
    property int step: 1;
    property string suffix: "";
    property int min: 0;
    property int max: 65535;
    property bool enabled: true;

    property int __oldVal: 0;
    property bool __initialized: false;

    signal changed(int value)

    FocusScope {
        id: focuScope;
        focus: true;

        Row {

            PlasmaComponents.TextField {
                id: input;

                anchors {
                    verticalCenter: parent.verticalCenter;
                }

                onAccepted: __accept();

                Keys.onUpPressed: {
                    __accept();
                    increase();
                }
                Keys.onDownPressed: {
                    __accept();
                    decrease();
                }
            }

            Column {
                id: buttons;

                PlasmaComponents.ToolButton {
                    id: upButton;
                    iconSource: "arrow-up";
                    onClicked: increase();
                    height: 16;
                    width: 16;
                }

                PlasmaComponents.ToolButton {
                    id: downButton;
                    iconSource: "arrow-down";
                    onClicked: decrease();
                    height: 16;
                    width: 16;
                }

            }
        }

        onActiveFocusChanged: __accept();
    }

    function __accept() {
        var v = parseInt(input.text);
        if ((v < min) || (v > max)) {
            input.text = parseInt(value) + suffix;
            return;
        }

        if (v != value)
            value = v;
    }

    function increase()
    {
        if ((value + step) <= max)
            value += step;
    }

    function decrease()
    {
        if ((value - step) >= min)
            value -= step;
    }

    onValueChanged: {
        print("Value: " + value + "   Min: " + min + "   Max: " + max);
        if ((value < min) || (value > max))
            return;

        input.text = parseInt(value) + suffix;

        if ((__oldVal != value) && (__initialized == true) && (enabled == true))
            changed(value);

        __oldVal = value;
        __initialized = true;

        upButton.enabled = ((enabled == true) && (value < max));
        downButton.enabled = ((enabled == true) && (value > min));
    }

    onMaxChanged: {
        if (value > max)
            value = max;

        upButton.enabled = ((enabled == true) && (value < max));
    }

    onMinChanged: {
        if (value < min)
            value = min;

        downButton.enabled = ((enabled == true) && (value > min));
   }

   onEnabledChanged: {
        input.enabled = enabled;
        if (enabled && (value < max))
            upButton.enabled = true;
        else
            upButton.enabled = false;

        if (enabled && (value > min))
            downButton.enabled = true;
        else
            downButton.enabled = false;
   }

}